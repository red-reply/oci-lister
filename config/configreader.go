package config

import (
	"fmt"
	"github.com/go-ini/ini"
	"os"
)

// OCIconfiguration is a extended common config provider
type OCIconfiguration struct {
	Tenancy              string
	User                 string
	Region               string
	Fingerprint          string
	PrivateKey           string
	PrivateKeyPassphrase string
	CompartmentID        string
}

// Getconfig reads a config file and crates a extended common config provider
func (r *OCIconfiguration) Getconfig() {
	cfg, err := ini.Load("oci-config.ini")
	if err != nil {
		fmt.Printf("Fail to read file: %v", err)
		os.Exit(1)
	}

	r.Tenancy = cfg.Section("DEFAULT").Key("tenancy").String()
	r.User = cfg.Section("DEFAULT").Key("user").String()
	r.Region = cfg.Section("DEFAULT").Key("region").String()
	r.Fingerprint = cfg.Section("DEFAULT").Key("fingerprint").String()
	r.PrivateKey = cfg.Section("DEFAULT").Key("privateKey").String()
	r.PrivateKeyPassphrase = cfg.Section("DEFAULT").Key("privateKeyPassphrase").String()
	r.CompartmentID = cfg.Section("DEFAULT").Key("compartment-id").String()

}
