package helper

import (
	"context"
	"github.com/oracle/oci-go-sdk/common"
	"github.com/oracle/oci-go-sdk/core"
	"github.com/oracle/oci-go-sdk/example/helpers"
	"local.prv/e.heim/oci-lister/config"
)

// listVcns i borrowed from the go-oci-sdk ... they don't export it there core.vnc
func listVcns(ctx context.Context, c core.VirtualNetworkClient, id *string) []core.Vcn {
	request := core.ListVcnsRequest{
		CompartmentId: id,
	}

	r, err := c.ListVcns(ctx, request)
	helpers.FatalIfError(err)
	return r.Items
}

// GetVcnsList takes context, a ConfigurationProvider an a pointer to the Compartment id and returns al list of VNC within the compartment
func GetVcnsList(ctx context.Context, config common.ConfigurationProvider, configFile *config.OCIconfiguration) []core.Vcn {
	c, err := core.NewVirtualNetworkClientWithConfigurationProvider(config)
	helpers.FatalIfError(err)
	vcnItems := listVcns(ctx, c, &configFile.CompartmentID)

	var result []core.Vcn
	for _, element := range vcnItems {
		result = append(result, element)

	}
	return result

}
