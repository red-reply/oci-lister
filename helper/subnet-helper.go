package helper

import (
	"context"
	"github.com/oracle/oci-go-sdk/common"
	"github.com/oracle/oci-go-sdk/core"
	"github.com/oracle/oci-go-sdk/example/helpers"
	"local.prv/e.heim/oci-lister/config"
)

// listSubnets i borrowed that form the core pkg
func listSubnets(ctx context.Context, c core.VirtualNetworkClient, compartmentid *string, vcnid *string) []core.Subnet {

	request := core.ListSubnetsRequest{
		CompartmentId: compartmentid,
		VcnId:         vcnid,
	}

	r, err := c.ListSubnets(ctx, request)
	helpers.FatalIfError(err)
	return r.Items
}

// GetSubnetsList takes context, a ConfigurationProvider an a pointer to the Compartment id, a vcn id and returns a slice
// with subnet structs
func GetSubnetsList(ctx context.Context, config common.ConfigurationProvider, configFile *config.OCIconfiguration, vcnid *string) []core.Subnet {
	c, clerr := core.NewVirtualNetworkClientWithConfigurationProvider(config)
	helpers.FatalIfError(clerr)

	subnets := listSubnets(ctx, c, &configFile.CompartmentID, vcnid)

	var result []core.Subnet
	for _, element := range subnets {
		result = append(result, element)
	}
	return result
}
