package helper

import (
	"context"
	"github.com/oracle/oci-go-sdk/common"
	"github.com/oracle/oci-go-sdk/core"
	"github.com/oracle/oci-go-sdk/example/helpers"
	"local.prv/e.heim/oci-lister/config"
)

// GetInstancesList takes context, a ConfigurationProvider, a pointer to the Compartment id and returns al list of instances within the compartment
func GetInstancesList(configProvider common.ConfigurationProvider, configFile *config.OCIconfiguration) []core.Instance {

	c, err := core.NewComputeClientWithConfigurationProvider(configProvider)
	helpers.FatalIfError(err)
	ctx := context.Background()
	request := core.ListInstancesRequest{}
	request.CompartmentId = &configFile.CompartmentID
	rawOut, err := c.ListInstances(ctx, request)
	helpers.FatalIfError(err)
	var result []core.Instance
	for _, element := range rawOut.Items {
		result = append(result, element)
	}
	return result
}
