package helper

import (
	"context"
	"fmt"
	"github.com/oracle/oci-go-sdk/common"
	"local.prv/e.heim/oci-lister/config"
)

// ListAllCli prints all vcn, subnets and instances in the compartment to sdt.out (cli)
func ListAllCli(configProvider common.ConfigurationProvider, configFile *config.OCIconfiguration) {
	vcnList := GetVcnsList(context.Background(), configProvider, configFile)
	fmt.Println("State of Comartment: ")
	fmt.Println("| Virtual cloud networks:")

	for i, vcn := range vcnList {
		fmt.Printf("|- Display Name:%20v, DNS Label: %10v, Lifecycle State: %10v, ID: %v\n",
			*vcn.DisplayName, *vcn.DnsLabel, vcn.LifecycleState, *vcn.Id)
		subnetList := GetSubnetsList(context.Background(), configProvider, configFile, vcnList[i].Id)
		fmt.Println("   |- Subnets:")
		for _, subnet := range subnetList {
			fmt.Printf("   |- Display Name: %16v, DNS Label: %10v, Lifecycle State: %10v, ID: %v\n",
				*subnet.DisplayName, *subnet.DnsLabel, subnet.LifecycleState, *subnet.Id)
		}

	}
	fmt.Println("\n\nInstances: ")
	instances := GetInstancesList(configProvider, configFile)
	for _, instance := range instances {
		fmt.Printf("|- Display Name:%16v, Availability Domain: %10v, Lifecycle State: %10v, ID: %v\n",
			*instance.DisplayName, *instance.AvailabilityDomain, instance.LifecycleState, *instance.Id)
	}
}
