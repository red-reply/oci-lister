package helper

import (
	"fmt"
	"github.com/oracle/oci-go-sdk/common"
	"github.com/oracle/oci-go-sdk/example/helpers"
	"local.prv/e.heim/oci-lister/config"
	"os"
)

// ArgsFlags wraps cli arguments
type ArgsFlags struct {
	Help  bool
	List  bool
	Wrong bool
}

// DisplayHelpText ...yeah you are right it deletes the internet by googleing google
func DisplayHelpText() {
	fmt.Println("\n\nusage: oci-lister [options]")
	fmt.Println("\noci-lister '-h' or '--help' or 'help'")
	fmt.Println("-> display this text")

	fmt.Println("\noci-lister list")
	fmt.Println("-> displays vcn, subnets and instances in given compartment (config file) ")

	fmt.Println("\n\nremember that within this directory oci_api_key.pem ond oci-config.ini is needed!")
	fmt.Println("also see: https://gitlab.com/red-reply/oci-lister/blob/master/README.md")
}

// EvalCliArgs switch cases the cli args
func (a *ArgsFlags) EvalCliArgs() {
	if len(os.Args) == 1 {
		DisplayHelpText()
		fmt.Println("\tno argument provided")
		os.Exit(2)
	}

	for _, arg := range os.Args[1:] {
		switch arg {
		case "help":
			a.Help = true
		case "-h":
			a.Help = true
		case "--help":
			a.Help = true
		case "list":
			a.List = true
		default:
			a.Wrong = true

		}

	}
}

// Control steers main process
func Control() {
	steer := new(ArgsFlags)
	steer.EvalCliArgs()

	if steer.Help == true {
		DisplayHelpText()
	}

	if steer.Wrong == true {
		DisplayHelpText()
		fmt.Println("\tWrong argument provided")
		os.Exit(2)
	}

	if steer.List == true {
		configFile := new(config.OCIconfiguration)
		configFile.Getconfig()

		configProvider, err := common.ConfigurationProviderFromFileWithProfile("oci-config.ini", "DEFAULT", configFile.PrivateKeyPassphrase)
		helpers.FatalIfError(err)

		ListAllCli(configProvider, configFile)
	}
}
