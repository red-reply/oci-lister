[![License](https://img.shields.io/badge/licens-MIT-green.svg)](https://opensource.org/licenses/MIT)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/red-reply/oci-lister)](https://goreportcard.com/report/gitlab.com/red-reply/oci-lister)
[![Go version](https://img.shields.io/badge/go-1.11-green.svg)](https://golang.org/)

# oci lister

query oracle cloud infrastructure compartment and list
* VNC
* subnets
* instances 
 
### Please consider this 'work in progress' ;)

## build:

1. install [go](https://golang.org/doc/install)
2. go get github.com/oracle/oci-go-sdk
3. go build -o oci-lister .

## download:
need to figure that one out ... <br>
maybe a *unix_amd64 precompiled binary.zip somewhere 

## prerequisite:
provide **private key** (oci_api_key.pem, see [OCI Keys documentation](https://docs.cloud.oracle.com/iaas/Content/API/Concepts/apisigningkey.htm) ) and **config file** [oci-config.ini](https://gitlab.com/red-reply/oci-lister/blob/master/oci-config.ini.dummy)</br>
with in the same directory as oci-lister!! <br>

## run:
oci-lister **list** <br>
-> list vnc, subnets and instances <br>
<br>
oci-lister **help** or **-h** or **--help** <br>
-> yeah! you got me, this will break the internet, by googleing google <br> 

